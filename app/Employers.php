<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employers extends Model
{
    protected $fillable = [
        'firstname', 'lastname', 'email', 'phone', 'company_id'
    ];

    public function company() {
        return $this->belongsTo('App\Companies');
    }
    
    public static function getEmployers(){
    	return Employers::paginate(2);
    }

    public static function getEmployer($id){
    	return Employers::find($id);
    }

    public static function saveEmployer($request){
        $employer = new Employers([
            'firstname'     => $request->get('firstname'),
            'lastname'      => $request->get('lastname'),
            'email'         => $request->get('email'),
            'phone'         => $request->get('phone'),
            'company_id'    => $request->get('company_id'),
        ]);
        $employer->save();
    }

    public static function updateEmployer($request, $id){
        $employer = Employers::find($id);
        $employer->firstname    = $request->get('firstname');
        $employer->lastname     = $request->get('lastname');
        $employer->email        = $request->get('email');
        $employer->phone        = $request->get('phone');
        $employer->company_id   = $request->get('company_id');
        $employer->save();
    }

    public static function removeEmployer($id){
        Employers::destroy($id);
    }
}
