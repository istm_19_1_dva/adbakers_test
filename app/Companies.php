<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    protected $fillable = [
        'name', 'email', 'logo', 'website'
    ];

    public function employers() {
        return $this->hasMany('App\Employers');
    }

    public static function getCompanies(){
    	return Companies::paginate(2);
    }

    public static function getCompany($id){
    	return Companies::find($id);
    }

    public static function saveCompany($request, $image){
        $company = new Companies([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'logo' => $image,
            'website' => $request->get('website'),
        ]);
        $company->save();
    }

    public static function updateCompany($request, $id, $image){
        $company = Companies::find($id);
        $company->name      = $request->get('name');
        $company->email     = $request->get('email');
        if($image){
            $company->logo  = $image;
        }
        $company->website   = $request->get('website');
        $company->save();
    }

    public static function removeCompany($id){
        Companies::destroy($id);
    }
}
