<?php

namespace App\Http\Helpers;


class Image
{
    public static function saveImage($file, $upload_dir)
    {
        $done_file = false;
        $file_name = $file['name'];
        $ext = substr($file_name, strpos($file_name,'.'), strlen($file_name)-1);
        $file_name = time() . $ext;
        if(move_uploaded_file($file['tmp_name'], $upload_dir . $file_name)){
            $done_file = realpath( $upload_dir . $file_name );
        }

        if($done_file){
            //Return name of image
            return $file_name;
        } else {
            return false;
        }
    }

    public static function updateImage($file, $old_file, $upload_dir)
    {
        $done_file = false;
        $file_name = $file['name'];
        $ext = substr($file_name, strpos($file_name,'.'), strlen($file_name)-1);
        $file_name = $main_image_src = time() . $ext;
        if(move_uploaded_file($file['tmp_name'], $upload_dir . $file_name)){
            $done_file = realpath( $upload_dir . $file_name );
        }
        if($done_file){
            if(file_exists($upload_dir . $old_file)){
                //unlink($upload_dir . $old_file);
            }
            return $file_name;
        } else {
            return false;
        }
    }
}