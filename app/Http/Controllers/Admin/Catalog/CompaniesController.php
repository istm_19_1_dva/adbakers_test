<?php

namespace App\Http\Controllers\Admin\Catalog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCompanies;
use App\Http\Helpers\Image;

use App\Companies;

class CompaniesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Companies::getCompanies();

        return view('admin.catalog.companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.catalog.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompanies $request)
    {
        $validated = $request->validated();

        $images = $_FILES;
        $image = Image::saveImage($images['logo'], config('app.image_upload_dir'));

        Companies::saveCompany($request, $image);

        return redirect('/admin_panel/companies')->with('success', 'Companies has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Companies::getCompany($id);

        return view('admin.catalog.companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCompanies $request, $id)
    {
        $validated = $request->validated();

        $images = $_FILES;
        $image = Image::updateImage($images['logo'], $request->get('old_image'), config('app.image_upload_dir'));

        Companies::updateCompany($request, $id, $image);

        return redirect('/admin_panel/companies')->with('success', 'Companies has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Companies::removeCompany($id);

        return redirect('/admin_panel/companies')->with('success', 'Companies has been removed');
    }
}
