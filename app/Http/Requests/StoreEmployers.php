<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployers extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'max:255|unique:employers,email,'.$this->employer,
            'phone' => 'max:255|required|unique:employers,phone,'.$this->employer,
            'company_id' => 'required',
        ];
    }
}
