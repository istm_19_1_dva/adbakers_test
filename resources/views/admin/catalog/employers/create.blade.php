@extends('admin.layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0 text-dark">Create Employer</h1>
        
        @if(session()->get('errors'))
            <div class="alert alert-danger">
            {{ session()->get('errors') }}  
            </div><br />
        @endif
        </div><!-- /.col -->
    </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

            <div class="col-sm-12">
                <div class="card card-primary">
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" method="post" action="{{ route('admin.employers.store') }}">
                    @csrf
                    <div class="card-body">
                    <div class="form-group">
                            <label for="name">First Name</label>
                            <input type="text" name="firstname" class="form-control" placeholder="Enter firstname" value="{{ old('firstname') }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Last Name</label>
                            <input type="text" name="lastname" class="form-control" placeholder="Enter lastname" value="{{ old('lastname') }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" name="email" class="form-control" placeholder="Enter email" value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Phone</label>
                            <input type="text" name="phone" class="form-control" placeholder="Enter phone" value="{{ old('phone') }}">
                        </div>
                        <div class="form-group">
                            <label>Select</label>
                            <select name="company_id" class="form-control">
                            @foreach($companies as $company)
                            <option value="{{$company->id}}">{{$company->name}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.main-content -->
        
@endsection
