@extends('admin.layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0 text-dark">Companies</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <a href="{{ route('admin.employers.create')}}" class="btn btn-success float-right">Add</a>
        </div>
    </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

            <div class="col-sm-12">
                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                    <thead>
                    <tr role="row">
                        <th tabindex="0" rowspan="1" colspan="1">Id</th>
                        <th tabindex="0" rowspan="1" colspan="1">First Name</th>
                        <th tabindex="0" rowspan="1" colspan="1">Last Name</th>
                        <th tabindex="0" rowspan="1" colspan="1">Email</th>
                        <th tabindex="0" rowspan="1" colspan="1">Phone</th>
                        <th tabindex="0" rowspan="1" colspan="1">Company</th>
                        <th tabindex="0" rowspan="1" colspan="2">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($employers as $employer)
                        <tr role="row" class="odd">
                            <td>{{$employer->id}}</td>
                            <td>{{$employer->firstname}}</td>
                            <td>{{$employer->lastname}}</td>
                            <td>{{$employer->email}}</td>
                            <td>{{$employer->phone}}</td>
                            <td>{{$employer->company()->first()->name}}</td>
                            <td><a href="{{ route('admin.employers.edit',$employer->id)}}" class="btn btn-primary">Edit</a></td>
                            <td>
                                <form action="{{ route('admin.employers.destroy', $employer->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th rowspan="1" colspan="1">Id</th>
                            <th rowspan="1" colspan="1">First Name</th>
                            <th rowspan="1" colspan="1">Last Name</th>
                            <th rowspan="1" colspan="1">Email</th>
                            <th rowspan="1" colspan="1">Phone</th>
                            <th rowspan="1" colspan="1">Company</th>
                            <th rowspan="1" colspan="2">Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        {{ $employers->links() }}
    </div>
</section>
<!-- /.main-content -->
        
@endsection
