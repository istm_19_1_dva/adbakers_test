@extends('admin.layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0 text-dark">Companies</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <a href="{{ route('admin.companies.create')}}" class="btn btn-success float-right">Add</a>
        </div>
    </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

            <div class="col-sm-12">
                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                    <thead>
                    <tr role="row">
                        <th tabindex="0" rowspan="1" colspan="1">Id</th>
                        <th tabindex="0" rowspan="1" colspan="1">Name</th>
                        <th tabindex="0" rowspan="1" colspan="1">Email</th>
                        <th tabindex="0" rowspan="1" colspan="1">Logo</th>
                        <th tabindex="0" rowspan="1" colspan="1">Website</th>
                        <th tabindex="0" rowspan="1" colspan="2">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($companies as $company)
                        <tr role="row" class="odd">
                            <td>{{$company->id}}</td>
                            <td>{{$company->name}}</td>
                            <td>{{$company->email}}</td>
                            <td>{{$company->logo}}</td>
                            <td>{{$company->website}}</td>
                            <td><a href="{{ route('admin.companies.edit',$company->id)}}" class="btn btn-primary">Edit</a></td>
                            <td>
                                <form action="{{ route('admin.companies.destroy', $company->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th rowspan="1" colspan="1">Id</th>
                            <th rowspan="1" colspan="1">Name</th>
                            <th rowspan="1" colspan="1">Email</th>
                            <th rowspan="1" colspan="1">Logo</th>
                            <th rowspan="1" colspan="1">Website</th>
                            <th rowspan="1" colspan="2">Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        {{ $companies->links() }}
    </div>
</section>
<!-- /.main-content -->
        
@endsection
