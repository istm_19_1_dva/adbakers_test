<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i < 4; $i++){
            for($j = 1; $j < 3; $j++){
                DB::table('employers')->insert([
                    'firstname' => 'Firstname'.$i.$j,
                    'lastname' => 'Lastname'.$i.$j,
                    'email' => 'email'.$i.$j,
                    'phone' => 'phone'.$i.$j,
                    'company_id' => $i,
                ]);
            }
        }
    }
}
