<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i < 4; $i++){
            DB::table('companies')->insert([
                'name' => 'Company'.$i,
                'email' => 'email'.$i,
                'logo' => 'logo'.$i,
                'website' => 'website'.$i,
            ]);
        }
    }
}
